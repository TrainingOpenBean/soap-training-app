#!/usr/bin/env sh

appDirectory="/opt/springboot"

artifactName=$(find ./target/ -type f -name '*.jar' -printf "%f\n")

#try to go switch to applicationDirectory
cd $appDirectory || exit

#backup old jar file
mv *.jar backup/
cp "$CI_PROJECT_DIR"/target/"$artifactName" .

#start server in nohuop
sudo service soap-training restart
timeout --preserve-status 20 tail -f logs/application.log; exit 0