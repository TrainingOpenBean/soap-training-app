package com.openbean.soaptraining;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class SoapTrainingApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(SoapTrainingApplication.class);
        springApplication.run(args);

        log.debug("application started");
    }
}
