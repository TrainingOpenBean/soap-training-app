package com.openbean.soaptraining.application;

import com.openbean.soaptraining.exception.ServiceFaultException;
import com.openbean.soaptraining.exception.SoapFaultCode;
import com.openbean.soaptraining.repository.UserRepository;
import com.openbean.soaptraining.ws.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Slf4j
@Component
public class UserManagementApp {
    private UserRepository userRepository;

    @Autowired
    public UserManagementApp(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public GetUserResponse getUser(GetUserRequest getUserRequest) {
        log.debug("getUser web method called");
        User requestor = userRepository.findUser(getUserRequest.getRequestor());
        if(requestor == null){
            throw new ServiceFaultException("ERROR", SoapFaultCode.USER0006);
        }

        log.debug("requestor found "+requestor.getId());
        GetUserResponse getUserResponse = new GetUserResponse();

        User foundUser = lookupUser(getUserRequest.getId(), getUserRequest.getUsername());

        if((requestor.getRole() == Role.ADMIN || requestor.getRole() == Role.MAINTAINER) && requestor.isStatus()) {
            getUserResponse.setUser(foundUser);
        } else {
            getUserResponse.setUserInfo(foundUser.getUserInfo());
        }
        return getUserResponse;
    }

    public CreateUserResponse createUser(CreateUserRequest createUserRequest) {
        log.debug("createUser web method called");
        User requestor = userRepository.findUser(createUserRequest.getRequestor());
        if(requestor == null){
            throw new ServiceFaultException("ERROR", SoapFaultCode.USER0006);
        }
        log.debug("requestor found "+requestor.getId());

        if((requestor.getRole() == Role.ADMIN || requestor.getRole() == Role.MAINTAINER) && requestor.isStatus()){
            boolean notCreatedUsers = false;
            List<User> createdUsers = new ArrayList<>();

            StringBuilder reasonText = new StringBuilder();

            for (UserInfo userInfo : createUserRequest.getUserInfo()) {
                log.debug("adding user"+userInfo.getUsername());

                if (userInfo.getUsername() == null || userInfo.getUsername().equals("")) {
                    notCreatedUsers = true;
                    if(reasonText.length() > 0){
                        reasonText.append(", ");
                    }
                    reasonText.append("user on position ")
                            .append(createUserRequest.getUserInfo().indexOf(userInfo))
                            .append(" does not contain name");
                } else if ( userRepository.getBlacklistedNames().indexOf(userInfo.getUsername()) > -1) {
                    notCreatedUsers = true;
                    if(reasonText.length() > 0){
                        reasonText.append(", ");
                    }
                    reasonText.append(userInfo.getUsername())
                            .append(" contains invalid name");
                } else if(userRepository.findUser(userInfo.getUsername()) != null) {
                    notCreatedUsers = true;
                    if(reasonText.length() > 0){
                        reasonText.append(", ");
                    }
                    reasonText.append(userInfo.getUsername())
                            .append(" already exists");
                }
                User createdUser = userRepository.addUser(userInfo);
                if (createdUser == null) {
                    log.warn("user not "+userInfo.getUsername()+" created.");
                    notCreatedUsers = true;
                    if(reasonText.length() > 0){
                        reasonText.append(", ");
                    }
                    reasonText.append(userInfo.getUsername())
                            .append(" already exists");
                } else {
                    log.debug("user created "+createdUser.getId());
                    createdUsers.add(createdUser);
                }
            }

            if(notCreatedUsers) {
                throw new ServiceFaultException("WARNING", SoapFaultCode.USER0004, reasonText.toString());
            }
            return new CreateUserResponse().withUser(createdUsers);
        } else {
            throw new ServiceFaultException("ERROR", SoapFaultCode.USER0003);
        }
    }

    public UpdateUserResponse updateUser(UpdateUserRequest updateUserRequest) {
        log.debug("updateUser web method called");
        User requestor = userRepository.findUser(updateUserRequest.getRequestor());
        if(requestor == null){
            throw new ServiceFaultException("ERROR", SoapFaultCode.USER0006);
        }
        log.debug("requestor found "+requestor.getId());

        if((requestor.getRole() == Role.ADMIN || requestor.getRole() == Role.MAINTAINER) && requestor.isStatus()){
            User foundUser = lookupUser(updateUserRequest.getId(), updateUserRequest.getUsername());
            boolean userInfoValid = false;
            userInfoValid = validateUserInfo(updateUserRequest.getUserInfo());
            if(!userInfoValid) return null;

            User updatedUser = userRepository.updateUser(foundUser, updateUserRequest.isStatus(), updateUserRequest.getRole(), updateUserRequest.getUserInfo());

            if(updatedUser == null) {
                throw new ServiceFaultException("ERROR", SoapFaultCode.USER0005);
            }
            return new UpdateUserResponse().withUser(updatedUser);
        } else {
            throw new ServiceFaultException("ERROR", SoapFaultCode.USER0003);
        }
    }

    private User lookupUser(Long id, String username) {
        String lookupId;
        User foundUser;
        if(id == null){
            if(username == null){
                throw new ServiceFaultException("ERROR", SoapFaultCode.USER0001);
            } else {
                lookupId = username;
                foundUser = userRepository.findUser(username);
            }
        } else {
            lookupId = id.toString();
            foundUser = userRepository.findUser(id);
        }

        if(foundUser == null){
            throw new ServiceFaultException("ERROR", SoapFaultCode.USER0002, lookupId);
        }

        return foundUser;
    }

    private boolean validateUserInfo(UserInfo userInfo){
        int validateEmailResult = userRepository.validateEmail(userInfo.getEmail());
        if(!userRepository.validateUsername(userInfo.getUsername())){
            throw new ServiceFaultException("ERROR", SoapFaultCode.VAL0001);
        } else if(validateEmailResult == -1){
            throw new ServiceFaultException("ERROR", SoapFaultCode.VAL0002);
        } else if(validateEmailResult == -2){
            throw new ServiceFaultException("ERROR", SoapFaultCode.VAL0003);
        }
        return true;
    }
}