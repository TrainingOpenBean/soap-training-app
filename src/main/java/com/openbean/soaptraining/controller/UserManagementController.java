package com.openbean.soaptraining.controller;

import com.openbean.soaptraining.application.UserManagementApp;
import com.openbean.soaptraining.ws.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/userManagement")
@Slf4j
public class UserManagementController {

    private UserManagementApp userManagementApp;

    @Autowired
    public UserManagementController(UserManagementApp userManagementApp){
        this.userManagementApp = userManagementApp;
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.GET, params = "id")
    public GetUserResponse getUser(@RequestParam(name = "id") Long id) {
        return userManagementApp.getUser(new GetUserRequest().withRequestor(1L).withId(id));
    }

    @RequestMapping(value = "/getUserFull", method = RequestMethod.POST)
    public GetUserResponse getUserFull(@RequestBody GetUserRequest request) {
        return userManagementApp.getUser(request);
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    public CreateUserResponse createUser(@RequestBody CreateUserRequest request) {
        return userManagementApp.createUser(request);
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    public UpdateUserResponse updateUser(@RequestBody UpdateUserRequest request) {
        return userManagementApp.updateUser(request);
    }
}
