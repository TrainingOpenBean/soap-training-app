package com.openbean.soaptraining.endpoint;

import com.openbean.soaptraining.application.UserManagementApp;
import com.openbean.soaptraining.ws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class UserManagementEndpoint {
    private static final String NAMESPACE_URI = "http://open-bean.com/trainings/usr-management-web-service";

    private UserManagementApp userManagementApp;

    @Autowired
    public UserManagementEndpoint(UserManagementApp userManagementApp){
        this.userManagementApp = userManagementApp;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserRequest")
    @ResponsePayload
    public GetUserResponse getUser(@RequestPayload GetUserRequest getUserRequest) {
        return userManagementApp.getUser(getUserRequest);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createUserRequest")
    @ResponsePayload
    public CreateUserResponse createUser(@RequestPayload CreateUserRequest createUserRequest) {
        return userManagementApp.createUser(createUserRequest);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateUserRequest")
    @ResponsePayload
    public UpdateUserResponse updateUser(@RequestPayload UpdateUserRequest updateUserRequest) {
        return userManagementApp.updateUser(updateUserRequest);
    }
}
