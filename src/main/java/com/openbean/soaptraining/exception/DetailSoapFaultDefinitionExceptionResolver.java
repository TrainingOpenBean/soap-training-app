package com.openbean.soaptraining.exception;

import com.openbean.soaptraining.ws.ServiceStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;

import javax.xml.namespace.QName;
import java.time.LocalDateTime;

@ControllerAdvice
public class DetailSoapFaultDefinitionExceptionResolver extends SoapFaultMappingExceptionResolver {

    private static final QName CODE = new QName("statusCode");
    private static final QName MESSAGE = new QName("message");

    @Override
    protected void customizeFault(Object endpoint, Exception ex, SoapFault fault) {
        logger.warn("Exception processed ", ex);
        if (ex instanceof ServiceFaultException) {
            ServiceStatus status = ((ServiceFaultException) ex).getServiceStatus();
            SoapFaultDetail detail = fault.addFaultDetail();
            detail.addFaultDetailElement(CODE).addText(status.getStatusCode());
            detail.addFaultDetailElement(MESSAGE).addText(status.getMessage());
        }
    }

    @ExceptionHandler(ServiceFaultException.class)
    public ResponseEntity<CustomErrorResponse> customHandleNotFound(Exception ex, WebRequest request) {

        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        if (ex instanceof ServiceFaultException) {
            ServiceStatus status = ((ServiceFaultException) ex).getServiceStatus();

            switch (status.getStatusCode()) {
                case "USER-0001":
                case "VAL-0001":
                case "VAL-0002":
                case "VAL-0003":
                    errors.setStatus(HttpStatus.BAD_REQUEST.value());
                    break;
                case "USER-0002":
                case "USER-0006":
                    errors.setStatus(HttpStatus.NOT_FOUND.value());
                    break;
                case "USER-0003":
                    errors.setStatus(HttpStatus.FORBIDDEN.value());
                    break;
            }

            errors.setErrorCode(status.getStatusCode());
            errors.setErrorMessage(status.getMessage());
        }
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);

    }
}
