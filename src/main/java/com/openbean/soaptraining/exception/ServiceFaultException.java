package com.openbean.soaptraining.exception;

import com.openbean.soaptraining.ws.ServiceStatus;

public class ServiceFaultException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private ServiceStatus serviceStatus;

    public ServiceFaultException(String message, ServiceStatus serviceStatus) {
        super(message);
        this.serviceStatus = serviceStatus;
    }

    public ServiceFaultException(String message, SoapFaultCode faultCode){
        super(message);
        serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode(faultCode.getFaultCode());
        serviceStatus.setMessage(faultCode.getFaultMessage());
    }

    public ServiceFaultException(String message, SoapFaultCode faultCode, String argument1){
        super(message);
        serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode(faultCode.getFaultCode());
        serviceStatus.setMessage(String.format(faultCode.getFaultMessage(), argument1));
    }

    public ServiceFaultException(String message, SoapFaultCode faultCode, String argument1, String argument2){
        super(message);
        serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode(faultCode.getFaultCode());
        serviceStatus.setMessage(String.format(faultCode.getFaultMessage(), argument1, argument2));
    }

    public ServiceFaultException(String message, Throwable e, ServiceStatus serviceStatus) {
        super(message, e);
        this.serviceStatus = serviceStatus;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

}
