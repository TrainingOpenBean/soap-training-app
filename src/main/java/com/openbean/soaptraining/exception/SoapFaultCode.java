package com.openbean.soaptraining.exception;

public enum SoapFaultCode {
    USER0001("USER-0001", "Searched Id or username missing in request."),
    USER0002("USER-0002", "User %1$s not found in database."),
    USER0003("USER-0003", "Your role does not permit this operation."),
    USER0004("USER-0004", "User(s) not created. reason: %1$s."),
    USER0005("USER-0005", "Update of user failed. Database reason."),
    USER0006("USER-0006", "Id or username of requestor is missing in request."),
    VAL0001("VAL-0001", "Username is missing in request"),
    VAL0002("VAL-0002", "Email addres is missing in request."),
    VAL0003("VAL-0003", "Invalid format of email addres.");

    private String faultCode;
    private String faultMessage;

    SoapFaultCode(final String faultCode, final String faultMessage) {
        this.faultCode = faultCode;
        this.faultMessage = faultMessage;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public String getFaultMessage() {
        return faultMessage;
    }
}
