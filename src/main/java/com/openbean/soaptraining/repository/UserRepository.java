package com.openbean.soaptraining.repository;

import com.openbean.soaptraining.ws.Role;
import com.openbean.soaptraining.ws.User;
import com.openbean.soaptraining.ws.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import org.apache.commons.validator.routines.EmailValidator;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class UserRepository {
    private static Map<Long, User> users = new HashMap<>();
    private static Map<String, Long> usernameToIdBinding = new HashMap<>();
    private static Long currentId;

    private static List<String> blacklistedNames = new ArrayList<>();

    @PostConstruct
    public void initData(){
        List<String> interests = new ArrayList<>();
        interests.add("administration");
        User superAdmin = new User()
                .withId(1)
                .withStatus(true)
                .withRole(Role.ADMIN)
                .withUserInfo(
                        new UserInfo()
                        .withUsername("superadmin")
                        .withEmail("superadmin@demoservice.com")
                        .withCountry("CZ")
                        .withInterests(interests));
        users.put(superAdmin.getId(), superAdmin);
        usernameToIdBinding.put(superAdmin.getUserInfo().getUsername(), superAdmin.getId());
        blacklistedNames.add("superadmin");
        blacklistedNames.add("superAdmin");
        blacklistedNames.add("Superadmin");
        blacklistedNames.add("SuperAdmin");

        setCurrentId(1L);
    }

    public User findUser(long id){
        return users.get(id);
    }

    public User findUser(String username){
        Long id = usernameToIdBinding.get(username);
        if(id == null) return null;
        return findUser(id);
    }

    public boolean storeUser(User user){
        if(user == null) {
            log.debug("user null");
            return false;
        }

        users.put(user.getId(), user);
        usernameToIdBinding.put(user.getUserInfo().getUsername(), user.getId());
        log.debug("user saved "+usernameToIdBinding.get(user.getUserInfo().getUsername()));
        return true;
    }

    public synchronized Long getNextId(){
        setCurrentId(currentId+1);
        log.debug("next user id is "+currentId);
        return currentId;
    }

    private static synchronized void setCurrentId(Long id){
        log.debug("setting next user id to "+id);
        currentId = id;
    }

    public User addUser(UserInfo userInfo) {
        if(userInfo.getEmail() == null){
            return null;
        }
        User createdUser = new User().withUserInfo(userInfo).withRole(Role.USER).withId(getNextId());

        log.debug("user instance created "+createdUser.getId());

        if(!storeUser(createdUser)) return null;

        return createdUser;
    }

    public User updateUser(User userToUpdate, boolean status, Role role, UserInfo updateUserInfo) {
        userToUpdate.setRole(role);
        userToUpdate.setStatus(status);
        userToUpdate.setUserInfo(updateUserInfo);

        if(!storeUser(userToUpdate)) userToUpdate=null;

        return userToUpdate;
    }

    public List<String> getBlacklistedNames() {
        return blacklistedNames;
    }

    public boolean validateUsername(String username) {
        return (username != null && !username.equals(""));
    }

    public int validateEmail(String email) {
        if(email == null || email.equals("")) return -1;
        if(!EmailValidator.getInstance().isValid(email)) return -2;
        return 0;
    }
}
