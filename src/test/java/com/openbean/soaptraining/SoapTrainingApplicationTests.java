package com.openbean.soaptraining;

import com.openbean.soaptraining.application.UserManagementApp;
import com.openbean.soaptraining.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SoapTrainingApplicationTests {

    @Autowired
    private UserManagementApp userManagementApp;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void contexLoads() throws Exception {
        assertThat(userManagementApp).isNotNull();
        assertThat(userRepository).isNotNull();
    }

}
